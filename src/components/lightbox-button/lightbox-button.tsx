import { Component, Prop, h } from "@stencil/core";
import { modalController } from "@ionic/core";

interface ImageSize {
  height: number;
  width: number;
}

@Component({
  tag: "se-lightbox-button",
})
export class LightboxButton {
  @Prop() image: string;

  async openModal(e: UIEvent) {
    let size = await this.getSize();
    if (size.width < 600 || size.height < 600) {
      window.open(this.image, "_blank");
    } else {
      const lightbox = document.createElement("se-lightbox");
      lightbox.image = this.image;
      const options = {
        cssClass: "se-lightbox-modal",
        component: lightbox,
        ev: e,
      };
      const modal = await modalController.create(options);
      modal.style.setProperty("--height", `${size.height}px`);
      modal.style.setProperty("--width", `${size.width}px`);
      await modal.present();
      return modal;
    }
  }

  getSize(): Promise<ImageSize> {
    return new Promise((resolve, reject) => {
      let img = new Image();
      img.onload = () => {
        let imgAspect = img.width / img.height;
        let windowAspect = window.innerWidth / (window.innerHeight - 56);
        let width: number, height: number;
        if (imgAspect > windowAspect) {
          width = window.innerWidth * 0.8;
          height = width / imgAspect;
        } else {
          height = window.innerHeight * 0.8;
          width = height * imgAspect;
        }
        resolve({
          height: height,
          width: width,
        });
      };
      img.onerror = reject;
      img.src = this.image;
    });
  }

  render() {
    return (
      <ion-button
        slot="end-buttons"
        title="Expand image"
        fill="clear"
        color="light"
        onClick={(e) => this.openModal(e)}
      >
        <ion-icon slot="icon-only" name="expand"></ion-icon>
      </ion-button>
    );
  }
}
