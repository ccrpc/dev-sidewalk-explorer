import { Component, Element, Prop, h } from "@stencil/core";

@Component({
  styleUrl: "graphic-header.css",
  tag: "se-graphic-header",
})
export class GraphicHeader {
  @Element() el: HTMLElement;

  @Prop() image: string;
  @Prop() primary: string;
  @Prop() secondary: string;
  @Prop() hasSelection: boolean = false;

  hostData() {
    return {
      style: {
        backgroundImage: this.image ? `url('${this.image}')` : "none",
      },
    };
  }

  render() {
    return [
      !this.image ? <p class="no-image">No image found.</p> : null,
      this.hasSelection ? <se-deselect-button></se-deselect-button> : null,
      <h2 class="gradient">
        <span class="primary">{this.primary}: </span>
        <span class="secondary">{this.secondary}</span>
      </h2>,
      this.image && this.hasSelection ? (
        <se-lightbox-button image={this.image}></se-lightbox-button>
      ) : null,
    ];
  }
}
