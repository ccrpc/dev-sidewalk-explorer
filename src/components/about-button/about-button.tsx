import { Component, Method, h } from "@stencil/core";
import { modalController } from "@ionic/core";

@Component({
  tag: "se-about-button",
})
export class AboutButton {
  @Method()
  async openModal(e: UIEvent) {
    const options = {
      component: document.createElement("se-about"),
      ev: e,
    };
    const modal = await modalController.create(options);
    await modal.present();
    return modal;
  }

  render() {
    return (
      <ion-button
        slot="end-buttons"
        title="About"
        onClick={(e) => this.openModal(e)}
      >
        <ion-icon slot="icon-only" name="help-circle-outline"></ion-icon>
      </ion-button>
    );
  }
}
