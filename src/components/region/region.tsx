import { Component, Prop } from "@stencil/core";

@Component({
  tag: "se-region",
})
export class Region {
  @Prop() baselineYear: string;
  @Prop() label: string;
  @Prop() name: string;
  @Prop() value: string;
  @Prop({ mutable: true }) bbox: string | number[];

  componentWillLoad() {
    if (typeof this.bbox === "string")
      this.bbox = this.bbox.split(",").map(parseFloat);
  }
}
