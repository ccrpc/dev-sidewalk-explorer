import { Component, Prop } from '@stencil/core';


@Component({
  tag: 'se-field-group'
})
export class FieldGroup {
  @Prop() label: string;

  render() {
    return null;
  }
}
