export interface State {
  region?: HTMLSeRegionElement;
  municipality?: HTMLSeMunicipalityElement;
  year?: HTMLSeYearElement;
  featureType?: HTMLSeFeatureTypeElement;
  field?: HTMLSeFieldElement;
}

export const colors = ["#2e80bf", "#80cee8", "#fcfc5d", "#ffb061", "#e61c1f"];

export function getScoreColor(score: number) {
  if (score > 90) return colors[0];
  if (score > 80) return colors[1];
  if (score > 70) return colors[2];
  if (score > 60) return colors[3];
  return colors[4];
}

export function getStateByNames(
  featureType: string,
  field: string,
  region: string,
  municipality: string,
  year: string
): State {
  let regionEl: HTMLSeRegionElement =
    document.querySelector(`se-region[name='${region}']`) ||
    document.querySelector("se-region");

  let municipalityEl: HTMLSeMunicipalityElement =
    document.querySelector(`se-municipality[name='${municipality}']`) ||
    document.querySelector("se-municipality");

  let yearEl: HTMLSeYearElement =
    regionEl.querySelector(`se-year[name='${year}']`) ||
    regionEl.querySelector("se-year");

  let featureTypeEl: HTMLSeFeatureTypeElement =
    document.querySelector(`se-feature-type[name='${featureType}']`) ||
    document.querySelector("se-feature-type");

  let fieldEl: HTMLSeFieldElement =
    featureTypeEl.querySelector(`se-field[name='${field}']`) ||
    featureTypeEl.querySelector("se-field");

  return {
    region: regionEl,
    municipality: municipalityEl,
    year: yearEl,
    featureType: featureTypeEl,
    field: fieldEl,
  };
}

export function getState(): State {
  let root = document.querySelector("se-root");
  return getStateByNames(
    root.featureType,
    root.field,
    root.region,
    root.municipality,
    root.year
  );
}

export function doOnce(key: string, action: Function) {
  let done = localStorage.getItem(key) === "true";
  if (!done) {
    localStorage.setItem(key, "true");
    action();
  }
}

export async function findOrCreate<E extends HTMLElement = HTMLElement>(
  tagName: string
): Promise<E> {
  let elm = document.querySelector<E>(tagName);
  if (elm == null) {
    elm = document.createElement(tagName) as E;
    document.body.appendChild(elm);
  }
  return Promise.resolve(elm);
}
