import { Component, Prop } from '@stencil/core';


@Component({
  tag: 'se-level'
})
export class Level {
  @Prop() label: string;
  @Prop() percent: boolean = true;
  @Prop() score: number;
}
