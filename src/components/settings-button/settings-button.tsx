import { Component, h } from "@stencil/core";
import { modalController } from "@ionic/core";

@Component({
  tag: "se-settings-button",
})
export class SettingsButton {
  async showSettings(e: UIEvent) {
    const options = {
      component: document.createElement("se-settings"),
      ev: e,
    };
    const modal = await modalController.create(options);
    await modal.present();
    return modal;
  }

  render() {
    return (
      <ion-button onClick={(e) => this.showSettings(e)} title="View options">
        <ion-icon slot="icon-only" name="settings"></ion-icon>
      </ion-button>
    );
  }
}
