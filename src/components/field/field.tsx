import { Component, Element, Prop, State, h } from "@stencil/core";
import { getState, State as AppState, getScoreColor, colors } from "../utils";

@Component({
  tag: "se-field",
})
export class Field {
  featureType?: HTMLSeFeatureTypeElement;
  levels?: HTMLSeLevelElement[];

  @Element() el: HTMLElement;

  @State() segment: "chart" | "table" = "chart";
  @Prop() baseline: number[];
  @Prop() current: number[];
  @Prop() label: string;
  @Prop() measure: string;
  @Prop() name: string;
  @Prop() summary: boolean = false;
  @Prop() value: string;
  @Prop() visible: boolean = false;

  componentWillLoad() {
    this.featureType = this.el.closest("se-feature-type");
    this.levels = Array.from(this.el.querySelectorAll("se-level"));
  }

  hostData() {
    return {
      style: {
        display: this.visible ? "block" : "none",
      },
    };
  }

  getYearLabel(state: AppState, prop: "baseline" | "current") {
    let year: HTMLSeYearElement =
      prop === "current"
        ? state.year
        : state.region.querySelector(
            `se-year[name='${state.region.baselineYear}']`
          );
    return year.name;
  }

  getTable(prop: "baseline" | "current") {
    let state = getState();
    let title = `${this.getYearLabel(state, prop)} Scores`;

    let values = this[prop];
    if (!values || !values.length) return;

    let ft = this.featureType;
    let header = this.measure.split(",");
    if (!this.summary) header.push("Score");
    header.push(ft.countLabel || ft.label);
    header.push(ft.percentLabel || `Percent of ${ft.label}`);

    let total = 0;
    this.levels.forEach((level, i) => {
      if (level.percent) total += values[i];
    });
    let valueFormat = {
      maximumFractionDigits: ft.value === "sidewalk" ? 1 : 0,
      minimumFractionDigits: ft.value === "sidewalk" ? 1 : 0,
    };
    let percentFormat = {
      maximumFractionDigits: 1,
      minimumFractionDigits: 1,
    };
    let rows = this.levels.map((level, i) => {
      let value = values[i];
      let row = level.label.split(",");
      if (!this.summary)
        row.push(isNaN(level.score) ? "\u2014" : level.score.toString());
      row.push(value.toLocaleString(undefined, valueFormat));
      row.push(
        `${((100 * value) / total).toLocaleString(undefined, percentFormat)} %`
      );
      return row;
    });
    rows.unshift(header);
    return (
      <rpc-table data={rows} tableTitle={title} textAlignment="l,r"></rpc-table>
    );
  }

  getBarColor(prop: "baseline" | "current"): string | string[] {
    if (prop === "baseline") return "#bbbbbb";
    if (this.summary) return colors;
    return this.levels.map((level) => getScoreColor(level.score));
  }

  getDataset(state: AppState, prop: "baseline" | "current") {
    let values = this[prop];
    if (!values || !values.length) return;

    return (
      <rpc-dataset
        type="bar"
        data={values}
        label={this.getYearLabel(state, prop)}
        borderWidth={0}
        backgroundColor={this.getBarColor(prop)}
      ></rpc-dataset>
    );
  }

  getChart() {
    let state = getState();
    return (
      <rpc-chart
        type="bar"
        labels={this.levels.map((level) => level.label.split(",")[0])}
        aspectRatio={1.1}
        chartTitle={`${this.label} Scores`}
        yLabel={state.featureType.countLabel || state.featureType.label}
        wrap={15}
      >
        {this.getDataset(state, "baseline")}
        {this.getDataset(state, "current")}
        <rpc-scale
          axis="x"
          type="category"
          autoSkip={false}
          scaleLabel={this.measure}
          min={0}
          gridLines={false}
        ></rpc-scale>
      </rpc-chart>
    );
  }

  render() {
    if (this.visible)
      return [
        <div>
          <slot />
          {this.getChart()}
          {this.getTable("current")}
          {this.getTable("baseline")}
        </div>,
      ];
  }
}
