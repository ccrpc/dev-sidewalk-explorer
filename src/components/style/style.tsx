import { Component, Prop, h } from "@stencil/core";
import {
  StyleSpecification,
  VectorSourceSpecification,
  LineLayerSpecification,
  CircleLayerSpecification,
  FillLayerSpecification,
  DataDrivenPropertyValueSpecification,
  ColorSpecification,
  FilterSpecification,
} from "maplibre-gl";
import { State } from "../utils";

@Component({
  tag: "se-style",
})
export class Style {
  @Prop() state: State;

  getSources(): { [_: string]: VectorSourceSpecification } {
    return {
      se: {
        type: "vector",
        url:
          "https://maps.ccrpc.org/sidewalk-explorer/" +
          `${this.state.year.value}.json`,
      },
    };
  }

  getLayers() {
    return [this.getFeatureLayer(), this.getAggregateLayer()];
  }

  getFeatureLayer(): CircleLayerSpecification | LineLayerSpecification {
    let featureLayer: CircleLayerSpecification | LineLayerSpecification = {
      id: this.state.featureType.value,
      source: "se",
      "source-layer": this.state.featureType.value,
      filter: this.getFilterExpression(),
      type: undefined,
    };

    if (this.state.featureType.value === "sidewalk") {
      featureLayer["type"] = "line";
      featureLayer["layout"] = {
        "line-cap": "round",
      };
      featureLayer["paint"] = {
        "line-color": this.getColorExpression(this.state.field.value),
        "line-width": ["interpolate", ["linear"], ["zoom"], 10, 0.25, 22, 7],
      };
    } else {
      featureLayer["type"] = "circle";
      featureLayer["paint"] = {
        "circle-color": this.getColorExpression(this.state.field.value),
        "circle-radius": ["interpolate", ["linear"], ["zoom"], 13, 4, 22, 8],
        "circle-stroke-width": 1,
        "circle-stroke-color": "#ffffff",
      };
    }

    return featureLayer;
  }

  getAggregateLayer(): FillLayerSpecification {
    return {
      id: "aggregated",
      type: "fill",
      source: "se",
      "source-layer": "aggregated",
      filter: this.getFilterExpression(),
      paint: {
        "fill-antialias": true,
        "fill-color": this.getColorExpression(
          `${this.state.featureType.value}_${this.state.field.value}`
        ),
        "fill-opacity": 0.5,
      },
    };
  }

  // second filter expression to get municipalities alone

  getFilterExpression(): FilterSpecification {
    if (this.state.municipality.label === "None") {
      return ["==", ["get", "region"], this.state.region.value];
    } else {
      return ["==", ["get", "municipality"], this.state.municipality.value];
    }
  }

  getFilterExpressionAggregate() {
    return ["==", ["get", "region"], this.state.region.value];
  }

  getColorExpression(
    field: string
  ): DataDrivenPropertyValueSpecification<ColorSpecification> {
    return [
      "case",
      ["has", field],
      [
        "step",
        ["get", field],
        "#e61c1f",
        60.001,
        "#ffb061",
        70.001,
        "#fcfc5d",
        80.001,
        "#80cee8",
        90.001,
        "#2e80bf",
      ],
      "#999999",
    ];
  }

  render() {
    let json: StyleSpecification = {
      version: 8,
      sources: this.getSources(),
      layers: this.getLayers(),
    };

    return (
      <gl-style
        json={json}
        id="app"
        clickableLayers={[
          "aggregated",
          "sidewalk",
          "curb_ramp",
          "crosswalk",
          "pedestrian_signal",
        ]}
      ></gl-style>
    );
  }
}
